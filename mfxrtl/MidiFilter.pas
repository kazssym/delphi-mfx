unit MidiFilter;

interface

uses System.Types;

{$ALIGN 1}
{$MINENUMSIZE 4}

const
  MFX_VERSION = 11;
{$NODEFINE MFX_VERSION}
{$HPPEMIT '#ifndef MFX_VERSION'}
{$HPPEMIT '#define MFX_VERSION 11'}
{$HPPEMIT '#endif'}

type
  IMfxEventQueue = interface;
  IMfxDataQueue = interface;

  IMfxEventFilter = interface(IUnknown)
    ['{8D7FDE01-1B1B-11D2-A8E0-0000A0090DAF}']
    function Connect(Context: IUnknown): HResult; stdcall;
    function Disconnect: HResult; stdcall;
    function OnStart(Time: LongInt; OutQueue: IMfxEventQueue): HResult; stdcall;
    function OnLoop(TimeRestart, TimeStop: LongInt; OutQueue: IMfxEventQueue)
      : HResult; stdcall;
    function OnStop(Time: LongInt; OutQueue: IMfxEventQueue): HResult; stdcall;
    function OnEvents(TimeFrom, TimeThru: LongInt;
      InQueue, OutQueue: IMfxEventQueue): HResult; stdcall;
    function OnInput(InQueue, OutQueue: IMfxDataQueue): HResult; stdcall;
  end;

  MfxData = record
    m_lTime: LongInt;
    case Integer of
      0:
        (m_dwData: DWORD);
      1:
        (m_byStatus, m_byData1, m_byData2: Byte);
  end;

  MfxMuteMask = Byte;

  MfxEvent = record

  type
    EventType = (Note, KeyAft, Control, Patch, ChanAft, Wheel, RPN, NPRN, Sysx);

    EventUnion1 = record
      case Integer of
        0:
          (m_byPort, m_byChan: Byte);
        1:
          (m_maskSet, m_maskClear: MfxMuteMask);
        2:
          (m_nOfs: ShortInt);
        3:
          (m_nTrim: ShortInt);
    end;

  var
    m_lTime: LongInt;
    m_u1: EventUnion1;
    case m_eType: EventType of
      Note:
        (m_byKey, m_byVel, m_byVelOff: Byte; m_dwDuration: DWORD);
      KeyAft:
        (m_byKeyAft, m_byAmtKey: Byte);
  end;

  IMfxDataQueue = interface(IUnknown)
    ['{7A37A621-1B1B-11D2-A8E0-0000A0090DAF}']
    function Add(const Data: MfxData): HResult; stdcall;
    function GetCount(out Count: Integer): HResult; stdcall;
    function GetAt(Index: Integer; out Data: MfxData): HResult; stdcall;
  end;

  MFX_HBUFFER = THandle;
  MFX_PHBUFFER = ^MFX_HBUFFER;

{$IF MFX_VERSION > 8}

  IMfxBufferFactory = interface(IUnknown)
    ['{C779B641-EDE2-11D2-A8E0-0000A0090DAF}']
    function Create(Length: LongWord; HBufNew: MFX_PHBUFFER): HResult; stdcall;
    function CreateCopy(HBufOld: MFX_HBUFFER; Length: LongWord;
      HBufNew: MFX_PHBUFFER): HResult; stdcall;
    function GetPointer(HBuf: MFX_HBUFFER; Data: PPointer; Length: PLongWord)
      : HResult; stdcall;
  end;

{$IFEND}

  IMfxEventQueue = interface(IUnknown)
    ['{FF76F7C2-F166-11D1-A8E0-0000A0090DAF}']
    function Add(const Event: MfxEvent): HResult; stdcall;
    function GetCount(out Count: Integer): HResult; stdcall;
    function GetAt(Index: Integer; out Event: MfxEvent): HResult; stdcall;
  end;

{$IF MFX_VERSION > 8}

  IMfxEventQueue2 = interface(IMfxEventQueue)
    ['{C51273A1-F0EA-11D2-A8E0-0000A0090DAF}']
    function GetBufferFactory(out BufferFactory: IMfxBufferFactory): HResult; stdcall;
  end;

{$IFEND}

implementation

end.
